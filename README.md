# Bash and Markdown 101

This repo is our documentation on bash and markdown. We will put the main commands of bash and document it in markdown. 

We will learn (unordered list - bulletpoints):
- bash
- markdown

First we will learn (numbered lists):
1. bash
2. markdown

## Bash subtitle

Bash is a language that speaks with the Kernel. It is used throughout Linux machines (90%)

### Main commands

Write here the **main commands**.

#### Where am I?

```bash
$ pwd
> /user/path/location
```

#### Where can I go?

```bash
$ ls
> directory_a   directory_b

$ ll
 Also shows hidden paths/directories
```

#### Go somewhere

```bash
$ cd directory
```

#### Come back

```bash
$ cd ..
```

#### Create a file

```bash
$ touch examplefile
```

#### Create a folder/directory

```bash
$ mkdir name
```

#### Remove a file

```bash
$ rm target
```

#### Remove a directory

```bash
$ rmdir directory

$ rm -rf directory
```

#### Printing to console

```bash
$ echo 'hello'
> hello
```

#### Truncate output into file

```bash
$ echo 'hello world' > examplefile
```

#### Appending to a file

```bash
$ echo 'Once upon a time..' >> examplefile
```

#### Type of command

```bash
$ type rm
> rm is /bin/rm

$ type brew
> brew is user/local/bin/brew

$ type cd
> cd is shell builtin
```

