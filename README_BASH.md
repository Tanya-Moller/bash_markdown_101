# BASH 


## Comments and Principles worth remembering
---
1. Everything in Linux is a file. e.g. keyboard, screen, files, directories.
2. Everything in Linux is a number. Files are recognised by I-node number per file systems, Process = Process ID (PID), users by user ID (UID), groups (GID).
3. Command syntax:    ```cmd -options arg1 arg2 argn```
4. Input and output are byte streams, don't care what the format is.
5. The shell performs many directives (actions) prior to running the command.
    * a. parses line entered into pigeon holes (array).
    * b. Filename generation (wildcards -   * ? [ ]).
    * c. Redirection > < >>.
    * d. Pipelines | (joining stdout to stdin of processes to pass data through memory).
    * e. Quotes protect shell characters and make them literal (" ' \\), also referred to as escaping.
    * f. Subshells replacement `$(cmd)` or `cmd`  - these run commands and then use the output.
    * g. Subshells `( cmd )` - runs the command in a separate process.
    * h. Variables `$PATH` - substitutes to the stored value, even inside double quotes `"$PATH"` (will store the format within the variable).
    * i. Looks to find where the command is using your `$PATH` variable.
    * j. Rewrites the command line with all substitutions.
    * k. Executes the command.
6. Getting help, use the man command or search the web with
flavour of linux (e.g. Fedora, CentOS, Ubuntu) then the command.
    * You can also type man and the command into your web browser too.

---
## Some questions
---

what would happen if

```bash 
$ ouch="ls -lF /tmp"
$ $ouch
```

Compared to

```bash
$ hmmm=$(ls -lF /tmp)
$ $hmmm
```

**Answer:** The first one runs, whereas the second one doesn't.

Second one stores the ouput into the variable

`"$hmmm" | grep tanya` --> this would keep the formatting of the grep
`echo $hmmm` substitutes the variable and then runs the command

```bash
$ touch rm x y z 
$ *
```
\* removed everything except rm 



---
## General Commands
---

#### End of input
```bash
ctl D
```

#### Kill process

```bash
$ kill -9 $(ps aux | grep "Process_you_want_to_kill" | awk {print $2}')
```

#### Who is online

```bash
$ who
```

#### Who you are logged in as 

```bash
$ id
```

#### Check your directory

```bash
$ pwd
```

#### Get to home directory

```bash
$ cd
```

#### Manual

```bash
$ man ls
```

#### List showing everything 

```bash
$ ls -la
```

#### current directory

```bash
$ ./ 
```

#### Copy 

```bash
$ cp 
```

#### Where a command is 

```bash
$ which
```

#### Find

```bash
$ find /

e.g.

$ find / -name '*.d' 2>/dev/null
```

#### Find modification time while excluding anything with /proc (for example) in it

```bash
$ find / -mtime -1 2> /dev/null | grep -v "/proc"
```
- The -1 after -mtime is 1hr * 24 hence in the last 1 day
- grep -v means grep inverse so exclude /proc from results

#### List all files in directory including hidden ones

```bash
$ ls -a
```

#### Show contents of file

```bash
$ cat
```

#### Create a file

```bash
$ touch filename
```

#### Create more than 1 directory in a single command

```bash
e.g. 
$ mkdir books books/nonfiction books/fiction
```

#### Count

```bash
$ wc -l /path (count number of lines)
$ wc -w (count number of words)
$ wc -m (count number of characters)

$ wc -l <path (doesnt output location)
```

#### What is the only character that cannot be used in a Linux/Unix file name

```bash
$ /
```

#### Give me 2 ways in which I can create a file name containing a space

```bash
$ \<space>
$ " "
```

#### How could I create a file called -r and how could I remove it?

```bash
$ touch ./-r
$ rm ./-r
```

#### Check file e.g. whether it's executable or not

```bash
$ file 
```

#### List all files in the /etc containing any of , - " ' \

```bash
$ ls /etc/*[,""''\\-]*
```
- The dash needs to be at the end otherwise it reads as a list
- Backslash needs 2 so it reads it as a character
---

## Pipelines and Redirection

---
#### Put a message in a file and then write the message to yourself with the write command

```bash
$ write user | cat filename
```

#### Merge contents of files into a new file

```bash
$ cat file* > filex
$ cat file* >> filex
```
#### Copy the contents of a file to your 'clipboard'

```bash
$ cat file | pbcopy
```
#### Finding data or text

```bash 
$ grep '^word'
```

- the ^ looks for the word at the beginning of the regular expression. Put it at hte end and it looks for it at the end

```bash
$ grep'^.*
```
- the . means any character * is any number of characters

#### Extended grep

```bash
egrep
``` 

#### See all processes running

```bash
$ ps aux
$ ps -ef
```

#### Find specific processes

```bash
$ ps aux | grep <whatever>
$ ps -ef | grep bash
```

#### Find data using grep

```bash
$ grep <info> /<path>
```

#### Looking at space free, anything mounted other disks
disk free

```bash
$ df -h (human readable format)
```
dev is usually a physical piece of information

#### Find out which directory is using a lot of diskspace
disk utility

```bash
$ sudo du -sh (summarize human reable form) *
```
- sudo allows you more superior access

---

## Environment Variables and Profiles
---
In this class we looked at variables in bash. This included:

### Setting a variable in memory
- Setting a variable using command line 
```bash
$ NAME=TANYA
```
- Calling a variable 
```bash
$ echo $NAME
```
- We logged out of our session in bash and logged back in
- Variable was no longer set

### Setting a variable in `.bash_profile`

```bash
$ export DRINK=WATER
```

### Making a file that called a variable (Child process)
- in a file called files
```BASH
$ #!/bin/bash

$ echo $NAME
$ echo $BOOK
$ echo $DRINK
$ echo 'Hello files'
```

### Morphing the variable using export inside the file

```BASH
$ cat files
$ #!/bin/bash

$ echo $NAME
$ echo $BOOK
$ echo $DRINK
$ echo 'Hellooooooo from files'

$ export DRINK=GIN

$ ~/hello
```

- made a new file called hello with the contents:

```bash
$ echo 'Reading from hello'
$ echo $DRINK
```

### Adding more variables to `.bash_profile`

```bash
$ export DRINK=WATER
```

### Notes on commands:

#### Show variables in environment 

```bash
$ env
```

#### Show path in environment

```bash
$ echo $PATH
```

- The path is the files any shell/terminal reads before opening
- This defines what programmes, variables, aliases it knows.

#### Set a variable

```bash
$ VARIABLENAME=VARIABLE
```

Need to set up variables to exist for child processes - otherwise, when you log out, they are removed.

```bash
$ nano file
```

```bash
$ #!/bin/bash
```
- This shows the file when to run that this is the interpretor to run the file
- Then add

```bash
export VARIABLENAME=VARIABLE
```

- Then run the file

#### Change permissions on a file

```bash
$ chmod u+x filename
```
- u means user
- x means execute

- Need to set the variables inside the BASH profile so it reads from there and applies to all users

```bash
$ export VARIABLENAME=VARIABLE
```
- Need to put this in .bash_profile otherwise when you exit, it won't have the variables saved

#### Run a script but dont fork (dont start a new process)

```bash
$ source 
```
 - run it within the current process
 - It updates the current process by pulling in the instructions from the file

#### Remove a variable

```bash
unset <variable>
```
Commands and aliases go into the rc, variables go in the profile
- `etc/profile` is system-wide
- `.bash_profile` is used to update the memory
- then do `source` or `. .bash_profile`

`.bashrc` adds things to the memory of the current process - inside here, we have commands that add features to memory (aliases). They don't update the environment memory.
- This is sourced if you type bash (doesn't get inherited from the parent - it is loaded from the .bashrc)

#### Current PID
```bash
echo $$
``` 

#### To find out more info on PID and PPID
```bash
ps -f
```

### Environment variables
- An environment variable is both local and global 

#### Look for an environment variable
 
 ```bash
 env | grep <variable>
 ```

- (if it is available to children)

#### Is the variable available in the current process

```bash
set | grep <variable>
```
- After this you can just export the variable name (not variable=variablename)

#### Exit the current process

```bash
exit
```

#### Making an alias - example

```bash
alias sheepsay='cowsay -f sheep.cow'
```
- An alias is a command but is not inherited with a fork (new child process)


du -sh <target>
find the file size