# Proxy
- A proxy is a server that hides the identity of the client.
- You connect to a server through a proxy.
- You connect to a proxy (traffic) and then the proxy makes the request to the server for you.
- The server doesn't know who the client is. It responds back to the proxy.

Benefits:
- Anonymity - masks or hides your IP address.
- Caching - Keeps a cache so it doesn't need to pull every request/file from the server - only things that change.
  - Helps with performance on static pages.
- Blocking unwanted sites - retains 404 versions.
- GeoFencing - certain clients can only access certain servers.
- Acts as a firewall.
- Security.

# Reverse Proxy

- In a proxy, the server doesn't know who the client is.
  - Protects the client.
- In a reverse proxy, the client doesn't know what server they are connecting to.
  - Protects the server.
- You connect the client to the reverse proxy on port 80. The reverse proxy then sends the traffic to a server (can be 1 of many) on port 8080 (internal network).

Benefits:
- Load balancing - Any client they connect, has no idea about the server they are connecting to.
  - A round robin is where it loops through the servers, sending traffic to each, hence, balancing the load.
- Caching - you can cache content that doesn't really change if the request is the same.
- Isolating internal traffic - You can run all the stuff in an internal DMZ zone 
  - can configure the machine to run containers, virtual machines, actual machines.
  - It is all internal - the port is not accessible to the client.
- Logging - which server is going down, up etc. 
  - Query the info from the reverse proxy.
- Canary Deployment - You have one server, for example, which has a slightly different version of the application. The code is the same but the content varies. For each request, a small proportion can see a different result.
  - Experiment with new features.
- Security - server IP address is masked.