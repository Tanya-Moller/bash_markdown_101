# AWS - Virtual Machine

## Making a key
#### Download key and move it into .ssh

```bash
$ mv ~/Downloads/TanyaMollerKey.pem ~/ .ssh/TanyaMollerKey.pem
```

#### Change permissions of the key file in terminal if permission denied

```bash
$ chmod 600 TanyaMollerKey.pem 
```
--- 

## Starting an EC2 in AWS
Add tag
- Key is Name 
- Add a value

Select your key in the last stage

Note: Port 80 is the main/standard port (e.g. google homepage)


#### Load the VM - Log in to server

```bash
ssh -i ~/.ssh/TanyaMollerKey.pem ec2-user@3.249.127.184
$ ssh -i <key/path> user@public-ip
```
- The -i specifies an identity file

## Installing apache2(Debian)/httpd(Redhat) server
---

#### Software download manager

```bash
yum
```

#### Downloading a reverse proxy

```bash
sudo yum install httpd
```

- httpd is an example - there are others
- A **reverse proxy** is a program that redirects
- Port 22 is for the terminal (SSH)
- Port 80 (http) and 443 (https) are usually for the web

#### Start and enable apache (httpd)

```bash
sudo systemctl start httpd
sudo systemctl status httpd
sudo systemctl restart httpd

$ sudo systemctl <action> <service>
```

- sudo stands for super user do
- So you can do anything in the machine unhindered
- Check if it's working by going to the IP

```bash
$ curl localhost
```

- Curl is used for testing or looking at unrendered text from a web request (client url)
- You get back raw output
- Used for automated testing to see if a service is available to us
- Check response from a web application
- Status checks
- localhost is a place in the machine where you see changes of a web server as they happen 

#### To edit the web server

```bash
$ cd /var/www/html
```

```bash
$ sudo nano index.html
```

OR 
```bash
$ sudo nano /var/www/html/index.html
```

#### Put in an image (in nano)

```bash
$ <img src="image address">
```

#### Look for package - see if it's installed
```bash
$ rpm -qa grep | <package>
$ rpm -qa grep | httpd
```
- q query the database
- a is all

#### Change port number so you can access it ??

Insert the text/contents
- Now the edited message will appear at the IP address.

#### Secure copy

```bash
scp -i ~/.ssh/TanyaMollerKey.pem webserver.sh ec2-user@54.78.139.250:webserver.sh
```

#### Check it's executable

```bash
ssh -i ~/.ssh/TanyaMollerKey.pem ec2-user@54.78.139.250 webserver.sh ls -a
```

#### Make it executable

```bash
$ ssh -i ~/.ssh/TanyaMollerKey.pem ec2-user@54.78.139.250 chmod +x webserver.sh
```

#### Load the code from the file

```bash
ssh -i ~/.ssh/TanyaMollerKey.pem ec2-user@54.78.139.250 ./webserver.sh
```

#### Remove apache

```bash
ssh -i ~/.ssh/TanyaMollerKey.pem ec2-user@54.78.139.250 sudo yum -y erase httpd
```

#### Manually add port to web server from terminal - example

```bash
cd /etc/httpd/conf
ls
> httpd.conf
sudo nano httpd.conf
```
- Add the port number to Listen

```bash
grep -i listen httpd.conf #Check it's there
sudo systemctl restart httpd #Restart the system

sudo nano /var/www/html/index.html #Add contents/text/image to web server
```

#### Doing the same without entering the VM in terminal

```bash
mkdir aws-ssh-prov #make a directory
cd aws-ssh-prov #Enter the directory
git init #Make a repository in git
```
- Make a VScode file called e.g. webserver.sh and insert the following to run apache and insert the text/image to the web server
  
```bash
#!/bin/bash

ssh -i ~/.ssh/TanyaMollerKey.pem ec2-user@54.78.139.250 '
sudo yum -y install httpd
sudo systemctl start httpd

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1> OMG WTF</h1>
<img src=\"https://i.pinimg.com/originals/5d/50/96/5d50964c25f8ce13e5531c60892c03dd.gif\">
_END_"
'
```
- The `<<_END_` part is a heredoc. This identifies the start and end of the here document - the input

```bash
chmod +x webserver.sh
ls -l
./webserver.sh
```

#### Doing the same but with Automation
- This will allow it all to be done with a single command from the terminal

```bash
#!/bin/bash

if (( $#  > 0 )) # If numerical variable is greater than 0 - must use 2 brackets
then
    hostname=$1 # The first input is 0 so this indicates the second input in the line
else
    echo "WTF: you must supply a hostname or IP address" 1>&2 # 1>&2 Redirects stdout to stderr.
    exit 1 # exit 1 because the exit must be a non-zero
fi 

ssh -o StrictHostKeyChecking=no  -i ~/.ssh/TanyaMollerKey.pem ec2-user@$hostname '
sudo yum -y install httpd
if rpm -qa | grep '^httpd-[0-9]' >/dev/null 2>&1
then
  sudo systemctl start httpd
else
    exit 1
fi

sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1> OMG WTF</h1>
<img src=\"https://i.pinimg.com/originals/5d/50/96/5d50964c25f8ce13e5531c60892c03dd.gif\">
_END_"
'
```
- `ssh -o StrictHostKeyChecking=no` is so it doesn't ask for the fingerprint thing
- 2>&1 is to combine stderr and stdout into the stdout stream for further manipulation

#### Extra notes

```bash
#!/bin/bash

echo "Command: $0"
echo "First Arg: $1"
echo "Number of Args $#"
echo "All Arguments $*"
```

`echo $?` gives the exit status - if it is 0, the command succeeded
- non-zero means it failed
- when you put an exit do e.g. exit 1 (non-zero means it failed)