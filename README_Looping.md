# Loops

## for_loop_bash.sh
nano

```bash
for VARIABLE in 1 2 4 7 10
do 
   	echo $VARIABLE
done

for PLACEHOLDER in {2..12}
do
  echo "Example of interpolation"
  echo "$PLACEHOLDER - hello"
done

# I have a list of IPs 
#LIST_IPS=('122.12.3.3' '123.123.123.4' '62.12.3.1')
LIST_IPS="12.12.12.1 12.3.4.1 43.23.2.1"
# I want to call my other script with this list of IPs 
#so that I can provision jspaint on several machines

set -xv
for IP_Tan in $LIST_IPS
do 
  /home/tanya/loops_bash/other_provision_script.sh $IP_Tan
done
```
## futher_provision_script.sh
nano

```bash
# I want to take in arguments
# I want to set them as a variable called IP
IP=$1

# I want to print the line 'provisioning IP' where ip is the actual ip that I got taken as argument
echo "provisioning $IP"
```

```bash
for i in 18.202.20.30 52.50.108.17
do
    ./webserver.sh $i
done
```
