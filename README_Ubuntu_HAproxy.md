# Ubuntu Machine

- We are going to set up a **load balancer** with **HA proxy** on a **Ubuntu** machine.
- HA proxy is a reverse proxy.
- A load balancer is a proxy server that distributes traffic between 2 or more servers.
  - It is a reverse proxy with a bit more configuration.

Create 2 redhat machines and load the code for httpd on both of them so you have 2 html

#### See the system log using
```bash
$ cd /var/log
$ less syslog
```

### Log in to ubuntu machine and install HA proxy
---

```bash
$ ssh -i ~/.ssh/TanyaMollerKey.pem ubuntu@54.229.78.136
```
- The user for ubuntu machines is `ubuntu` not `ec2-user`.

Instructions for the installation: https://haproxy.debian.net/#?distribution=Ubuntu&release=bionic&version=2.2

```bash
$ sudo apt-get install --no-install-recommends software-properties-common
$ sudo apt update

$ sudo add-apt-repository ppa:vbernat/haproxy-2.2
$ sudo apt-get install haproxy=2.2.\* -y

$ sudo systemctl start haproxy
$ sudo systemctl status haproxy
```

We want it to listen on port 80 for any IP (*:80) and send information (redirect) to the other 2 machines (servers)

### Setting up the load balancer
---
Instructions from: https://www.digitalocean.com/community/tutorials/how-to-use-haproxy-to-set-up-http-load-balancing-on-an-ubuntu-vps

Rename the old configuration file and create a new one with the name haproxy.cfg.
- We need to edit this file with the necessary configuration for the HA proxy.

```bash
$ sudo nano haproxy.cfg
```
#### This is the code entered into the haproxy.cfg file
```bash
global
    log 127.0.0.1 local0 notice
    maxconn 2000
    user haproxy
    group haproxy

defaults
    log     global
    mode    http
    option  httplog
    option  dontlognull
    retries 3
    option redispatch
    timeout connect  5000
    timeout client  10000
    timeout server  10000

frontend simple_load_balancer
    bind *:80
    mode http
    option httpclose
    option forwardfor
    default_backend simple_servers

backend simple_servers
    balance roundrobin
    server lamp1 172.31.4.250:80
    server lamp2 172.31.10.78:80
```
#### Restart the server

```bash
$ sudo systemctl restart haproxy
$ sudo systemctl status haproxy
```
### Notes
---
#### Location of logs

```bash 
$ less var/log/syslog
```

#### Started the service manually specifying file to output

```bash
$ sudo proxy -f <file>
```


- haproxy worked with setting up sections: frontend and backend
- binding of ip needs to be done using keyboard bind
- ip binding can use wildcards

- Ubuntu systems log in with ubuntu user
- `atp-get` might need the repo of available packages to be updated `atp update`
  - still works with `-y`
- `systemctl` still works in ubuntu
- Still bash environment do local variables would probably be in a similar location.

### Networking issues
- Subnets talk via a route table
- There are security groups around each individual server
- Check the NACLs and route tables
- Check the security groups