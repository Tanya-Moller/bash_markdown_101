# System Admin
## Adding users to the system

#### Create a user
```bash
$ sudo useradd -m <name>
```

#### Give exit status of the last run command

```bash
$ echo $?
```

#### Check users that have been created in home by default

```bash
$ ls /home
$ grep <user> /etc/passwd
```

#### Give it a password
```bash
$ sudo passwd <user>
```

#### Switch user and check you switched
```bash
$ su - <user>
$ id
```
- The `-` means you become that user fully - without it, you stay in your current directory, not in theirs

#### Getting into sshd (ssh demon) and allow passwords
```bash
$ cd /etc/ssh
$ sudo nano sshd_config 

# Change PasswordAuthentication yes - this makes the machine hackable so you wouldn't do this

$ sudo systemctl reload sshd

$ sudo lsof -p <PID>
```
- `reload` just updates it as opposed to restart

#### Login as the new user

```bash
$ ssh <user>@<IP>
```

#### Allow your user to do everything

```bash
# login as the main user first

```

Create a file in sudoers

```bash
$ sudo ls /etc/sudoers.d
$ sudo cat /etc/sudoers.d/90-cloud-init-users
$ sudo nano /etc/sudoers.d/91-tanya
#Write: tanya ALL=(ALL)	ALL

```
- so they can run any command they want
- They will need to use their password
- first column is the user we allow to elevate theur privelig
- first ALL would usually be an IP or a hostname (restrict by the host or connecting hosts you are allowed to login from)
- In the (), is the user that you wish to become - in this case, you can sudo as any user `(-u <user>)`
- Last ALL means you can run any command

#### R 

```bash
# make a file called ~/.ssh/config
$ man ssh_config
```

## Kernel Tuning
- Tune for speed of delivery from the file system, databases etc.
- shmmax 
- To get the best performance out of the machine.
- It means you lock it to that specific application.
- Incorrect tuning can break the machine.

#### Kernel processes

```bash
$ cd /proc
$ ls
#e.g.
$ cat meminfo 
$ cd kernel
$ ls
```
- Find information about memory in the system using `meminfo` 
- Another is `diskstats` for example
- Get information from here and change it

#### e.g. turn on ip forwarding

```bash
$ cd /proc
$ cd sys/net/ipv4
$ sudo sh -c 'echo "1" >ip_forward'
```
or do it in the directory with sysctl

```bash
$ sudo sysctl net.ipv4.ip_forward=0
```

#### Tweak kernel attributes 

```bash
$ sudo sysctl
$ sudo sysctl -a | less
# input /ip_forward
```

```bash
$ sudo sysctl -a | less 
```
- lists all attributes

####

```bash
$ sudo less /etc/sysctl.d/00-defaults.conf
```
- sysctl.d this is the kernels equivalent of a profile
- to set them permanently

#### Setting specific values permanently in the kernel

Example

```bash
$ sudo su -
$ cd /etc/sysctl.d
$ sysctl net.ipv4.ip_forward
$ nano 01-network.conf
# In there, write net.ipv4.ip_forward=1
$ sysctl -p /etc/sysctl.d/01-network.conf
$ sudo init 6 
#log back in
$ sudo sysctl net.ipv4.ip_forward
```
- Change how the kernel is working with things there
- Kernel **attributes** - use `sysctl`
- `sudo su -` means log in as root
- `-w` means write the kernel property change to the file
- the file (we create it) must have .conf for it to be picked up
- `-p` means load the properties for this file
- `sudo init 6` means shutdown the machine and reload (reboot)

- **Device driver** - device that needs to be reworked 
- Driver causing a problem - `rmmod` to remove it
- `insmod`, `rmmod`
- `.ko` - kernel object

## File systems

- storage is in `/dev`
- Can add another disk on the device on AWS 
  - elastic block store emulates a physical hard disk.
  - Go to volumes --> Create volume
  - SSD (Solid State Drive) - Type of RAM but the data remains when they are powered off
  - IOPS - input output per second (faster) - send larger amounts of data per second to the disk
  - gp are fixed in this respect
  - Attach volume
- A snapshot is a backup of an existing VM

#### Check the new disk is there

```bash
sudo fdisk -l
```
- List the disks on the system
- A disk must have a partition to use it on a linux machine

#### Partition the disk (interactive)

```bash
sudo fdisk /dev/xvdf
n
```
- non interactive would be `svdf`
- n is new partition
- w saves the partition table to the disk
  
#### Inform the system of partitions

```bash
$ sudo partprobe
$ sudo partprobe -s
```
  
#### Formatting the partition

```bash
$ sudo mkfs -t ext3 /dev/xvdf1
```  
- `sudo mkfs` make file system
- `-t` is the type of the file system
- ext3 is an example for a type of linux file system
- format partition 1 of the xvdf disk


#### Mounting a disk

```bash
sudo mkdir /spare
sudo mount /dev/xvdf1 /spare
df -h
cd /spare/
sudo sh -c 'echo "My nice new file" >newfile'
```  
- Adding it to a directory called spare (which is a part of xvda1)
- Mount is bring the formatted file system online so you can save files to it - mounting is temporary (if you logoff have to redo it)
- Once mounted, whatever you now write to spare, it will be added to xvdf1 (the file system on xvdf)

#### Unmounting a disk

```bash
cd
sudo umount /spare
ls /spare
```  
- Takes the file system offline
- Then /spare is no longer there.
- So when you mount, the data goes onto the new directory but the data is always stored on the disk
- so when you unmount, you keep all the files
- If there was stuff already in the directory, it becomes hidden when you mount the disk
  
#### Mounting a disk permanently

```bash
sudo blkid
man fstab #to check what the fields means
sudo nano /etc/fstab
```  
- `fs tab` is file system table
- `sudo blkid` finds the UUID (unique identity)
- Paste in the UUID of the partition disk from `sudo blkid` and the file type (e.g. `ext3`)
- Then write default for example
- The first 1 is so that it dumps anything written on the file system (written to the disk) 
  - If it was another server, you'd put 0 (1 is true - updates the file system prior to shutting down)
- The 2 is to check this one after you check the root file system (The order of the file system check)
- Reboot the system with sudo init 6

#### 

```bash

```  
#### 

```bash

```  
#### 

```bash

```