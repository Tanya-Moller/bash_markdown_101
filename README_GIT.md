# Documenting GIT and BitBucket and Other

Git is a version control software. It manages version control so you do not end up with **Final_final_final_final_version2.docx**

It does this by creating timelines

### Commands

#### To start a repo 

```bash
$ git init
```

#### Git status

```bash
$ git status
```

#### Add to be tracked
```bash
$ git add .
```

(. means everything in there)

#### Committing to timeline

```bash
$ git commit -m
```

#### Git log

```bash
$ git log
```

#### Checkout timeline

```bash
$ git checkout
```

#### Checkout master

```bash
$ git checkout master
```

```bash
git push origin master
```
This pushes it to the origin from the master branch (our timeline is the master timeline)

#### To update on Bitbucket

```bash
$ git add .
$ git commit -m "meaningful message"
$ git push
```

```
$ git remote --v
```

- Create a repository
- Copy the line to add the origin to terminal
- Then git push origin master



